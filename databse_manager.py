import psycopg2  # required for DataBase Connectivity
from datetime import datetime  # to get datetime
import json

'''
To start server of Postgres
cd C:\Program Files\PostgreSQL\13\bin
pg_ctl.exe restart -D "C:\Program Files\PostgreSQL\13\data"
'''

# Reading config.json to parse to connection establishment
with open("config.json", "r") as file:
    config_data = json.load(file)

# Establishing the connection
try:
    conn = psycopg2.connect(
        database=config_data["DataBase_configuration"]["database_name"],
        user=config_data["DataBase_configuration"]["user"],
        password=config_data["DataBase_configuration"]["password"], host=config_data["DataBase_configuration"]["host"],
        port=config_data["DataBase_configuration"]["port"]
    )
except:
    print("Database Not Connected!")


class Database:
    def __init__(self):
        # Creating a cursor object using the cursor() method
        self.cursor = conn.cursor()

    def create_table(self):
        try:
            self.cursor.execute(
                "CREATE TABLE IF NOT EXISTS cookies (id SERIAL PRIMARY KEY, account_name VARCHAR(255), status VARCHAR(50), followers_file VARCHAR(150), following_file VARCHAR(150), created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP);"
            )
            conn.commit()
        except Exception as e:
            print(e)
            print("I can't drop our cookies database!")

    def insert_data_to_db(self, account_name, status, file_followers, file_following):
        self.cursor.execute("INSERT INTO cookies (account_name, status, followers_file, following_file) VALUES (%s, %s, %s, %s)", (account_name, status, file_followers, file_following))
        conn.commit()

    def fetch_data_of_db(self):
        self.cursor.execute("SELECT * FROM cookies")
        data = self.cursor.fetchall()
        return data

    def update_followers(self, which_row, what):
        self.cursor.execute("UPDATE cookies SET followers_file = %s WHERE id = %s", (what, which_row))
        conn.commit()

    def update_followings(self, which_row, what):
        self.cursor.execute("UPDATE cookies SET following_file = %s WHERE id = %s", (what, which_row))
        conn.commit()

    def update_status(self, id, status):
        self.cursor.execute("UPDATE cookies SET status = %s WHERE id = %s", (status, id))
        conn.commit()

    def drop_table(self):
        self.cursor.execute("DROP TABLE IF EXISTS cookies")
        conn.commit()

# instance = Database()
# instance.drop_table()
# instance.create_table()
# instance.insert_data_to_db("adidasoriginal", "not_started","Nothing", "Nothing")
# instance.update_db('status', 1)


# # Closing the connection
# conn.close()
# # print("Connection established to: PostgreSQL 11.5, compiled by Visual C++ build 1914, 64-bit")
