# Importing libraries
import requests
import json
import csv
import pandas as pd
import time
import json
import csv
import os
import glob


# https://www.instagram.com/adidasoriginals/?__a=1

def get_user_id_for_followings(username, myCookies):
    userid_url = f'https://www.instagram.com/{username}/?__a=1'
    headers = {
        'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 105.0.0.11.118 (iPhone11,8; iOS 12_3_1; en_US; en-US; scale=2.00; 828x1792; 165586599)'
    }
    usreid_page = requests.get(userid_url, cookies=myCookies, headers=headers)
    userid = usreid_page.json()['graphql']['user']['id']
    return userid


def get_followings(myCookies, userid, user_name):
    try:
        try:
            os.mkdir(f'{user_name}_response_txt')
        except:
            None
        max_id = ""
        page_count = 0
        for i in range(1000000000):
            if max_id is not "":
                url = f'https://i.instagram.com/api/v1/friendships/{userid}/following/?count=1200000&max_id=' + max_id
            else:
                url = f'https://i.instagram.com/api/v1/friendships/{userid}/following/?count=1200000'
            cookies = myCookies
            headers = {
                'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 105.0.0.11.118 (iPhone11,8; iOS 12_3_1; en_US; en-US; scale=2.00; 828x1792; 165586599)'
            }
            page_count += 1
            print('------------------------')
            print(f'page_count: {page_count}')
            page = requests.get(url, cookies=cookies, headers=headers)
            print('------------------------')
            print('------------------------')
            f = open(f'{user_name}_response_txt/' + str(i) + "following.txt", "w+", encoding="utf-8")
            f.write(page.text)
            f.close()
            max_id = page.json().get("next_max_id", None)
            print(f'max_id: {max_id}')
            print('------------------------')
            time.sleep(1)
    except Exception as error:
        print(error)
        data = []
        dir_path = f'{user_name}_response_txt'
        file_count = len(glob.glob1(dir_path, "*following.txt"))
        for i in range(file_count):
            file = open(f'{user_name}_response_txt/' + str(i) + "following.txt", "r+", encoding="utf-8")
            json_d = json.load(file)['users']
            data = data + json_d

        keys = data[0].keys()
        print("******************************************")
        print(keys)
        print("******************************************")
        with open(f'CSV_files/{user_name}_followings.csv', 'w', newline='', encoding="utf-8") as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(data)


# def main():
#     current_cookies = {
#         "ig_did": "41F34321-5B49-473E-9A49-9A2A38C22E6F",
#         "ig_nrcb": "1",
#         "mid": "YMGSgQALAAEmG0hoGAyftCXnD3KY",
#         "datr": "cNPmYGeoXSYObo1ppEjG3YGu",
#         "sessionid": "48327740494%3AYznkcf5dWXQtu0%3A24",
#         "csrftoken": "agkKSlFCaHjbHm7aIiHASxU0NEN073Vm",
#         "ds_user_id": "48327740494"
#     }
#     user_id = get_user_id_for_followings('k11musea', current_cookies)
#     get_followings(current_cookies, user_id, 'k11musea')
# main()