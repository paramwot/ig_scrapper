from flask import Flask, render_template, request, redirect, url_for, send_file
from databse_manager import Database
import os
import threading
from scrapping_manager import scrapper_main
from login_rpa import main

database_obj = Database()
database_obj.create_table()
app = Flask(__name__)
base_dir = os.getcwd()

@app.route("/", methods=['GET'])
def show():
    data = database_obj.fetch_data_of_db()
    new_data = []
    for record in data:
        current_record = {"id": record[0], "user_name": record[1], "status": record[2], "followers_file": record[3],
                          "following_file": record[4], "timestamp": record[5]}
        new_data.append(current_record)
    return render_template("index.html", results=new_data)


@app.route("/", methods=['POST', 'GET'])
def submit():
    database_obj.create_table()
    if request.method == "POST":
        url_string_list = request.form['url_input'].split(",")
        for name in url_string_list:
            name = name.split("/")[-2]
            print(name)
            database_obj.insert_data_to_db(name, "Not Started", "Nothing_to_show", "Nothing_to_show")
        return redirect(url_for("show"))
    return render_template("index.html")

@app.route("/download/<filename>")
def download(filename):
    file_path = os.path.join(base_dir, 'CSV_files', filename)
    print(file_path)
    return send_file(file_path, as_attachment=True, attachment_filename='')



if __name__ == "__main__":
    thread_for_scrapping = threading.Thread(target=scrapper_main)
    thread_for_scrapping.daemon = True
    thread_for_scrapping.start()
    # thread_for_login = threading.Thread(target=main)
    # thread_for_login.daemon = True
    # thread_for_login.start()
    app.run(debug=True, threaded=True)
