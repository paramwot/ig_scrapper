import json
from databse_manager import Database
from followers import *
from followings import *
import time
database_obj = Database()



def main(data):
    print("Inside main of scrapping_manager.py")
    # Reading cookies file
    with open("cookies.json", 'r') as file:
        cookies_data = json.load(file)
    print("read cookies.json")
    keys_from_cookies_json = list(cookies_data.keys())

    current_cookies = ''
    print("checking record from database")
    for record in data:
        current_record = {"id": record[0], "user_name": record[1], "status": record[2], "followers_file": record[3],
                          "following_file": record[4], "timestamp": record[5]}
        if current_record['status'] == "Not Started":
            database_obj.update_status(current_record['id'], "started")
            for key in keys_from_cookies_json:
                print(cookies_data[key]['status'])
                if cookies_data[key]['status']:
                    if not cookies_data[key]['In_use']:
                        current_cookies = cookies_data[key]['cookies']
            user_id = get_user_id(current_record['user_name'], current_cookies)
            print(f"Got user_id: {user_id}")
            print(f"started scrapping for: {current_record['user_name']}")
            get_followers(current_cookies, user_id, current_record['user_name'])
            database_obj.update_status(current_record['id'], "Done")
            get_followings(current_cookies, user_id, current_record['user_name'])

        if current_record['status'] == 'Done':
            print(f"*********status: {current_record['status']} for user_name: {current_record['user_name']} ")
            download_link_followers = f"http://127.0.0.1:5000/download/{current_record['user_name']}_followers.csv"
            database_obj.update_followers(current_record['id'], download_link_followers)
            print("************" + download_link_followers + "****************")
            download_link_followings = f"http://127.0.0.1:5000/download/{current_record['user_name']}_followings.csv"
            database_obj.update_followings(current_record['id'], download_link_followings)
        else:
            download_link = f"http://127.0.0.1:5000/"
            database_obj.update_followers(current_record['id'], download_link)
            database_obj.update_followings(current_record['id'], download_link)



def scrapper_main():
    while True:
        data = database_obj.fetch_data_of_db()
        main(data)
        time.sleep(1)
